use std::fmt::{Debug, Display};
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::str::FromStr;
use log::{debug, warn};
use serde::Deserialize;
use std::env;

#[derive(Debug, Clone, PartialEq)]
pub struct Config {
    pub account_id: String,
    pub password: String,
    pub record_id: String,
    pub domain_id: String,
    pub domain_name: String,
    pub domain_ttl: u64,
    pub ip_validate_path: String,
    pub ip_valid_response: String,
    pub ip_fetch_path: String,
    pub secondary_ip_fetch_path: String,
    pub ip_check_interval_seconds: u64,
    pub ip_check_retry_interval_seconds: u64,
}

#[derive(Debug, Clone, Deserialize, Default)]
pub struct ConfigFile {
    pub account_id: Option<String>,
    pub password: Option<String>,
    pub record_id: Option<String>,
    pub domain_id: Option<String>,
    pub domain_name: Option<String>,
    pub domain_ttl: Option<u64>,
    pub ip_validate_path: Option<String>,
    pub ip_valid_response: Option<String>,
    pub ip_fetch_path: Option<String>,
    pub secondary_ip_fetch_path: Option<String>,
    pub ip_check_interval_seconds: Option<u64>,
    pub ip_check_retry_interval_seconds: Option<u64>,
}

fn merge_config_sources<T: FromStr + Display>(environment_name: &str, config: Option<T>, default: Option<T>) -> T {
    if let Ok(environment) = env::var(environment_name) {
        debug!("Trying to parse {environment_name} from environment variable: {environment}");
        let environment = T::from_str(environment.as_str());
        debug!("Parsing from string: {}", environment.is_ok());
        if let Ok(environment) = environment {
            debug!("Parsed as {environment}");
            return environment;
        }
    }
    if let Some(config) = config {
        debug!("Pulled {environment_name} from configuration: {config}");
        return config;
    }
    if let Some(default) = default {
        debug!("Defaulting {environment_name} value: {default}");
        return default;
    }
    panic!("Failed to load value for {environment_name}!");
}

impl Config {
    /// Loads configuration from `config.toml` and parses it into a `Config` struct. Returns `Some()` on success and `None` on error
    fn load_config_file<P: AsRef<Path>>(path: P) -> ConfigFile {
        let mut file = match File::open(path) {
            Ok(file) => file,
            Err(e) => {
                warn!("Failed to open configuration file!\n{e}");
                return ConfigFile::default();
            }
        };
        let mut string = String::new();
        if let Err(e) = file.read_to_string(&mut string) {
            warn!("Failed to read config file to string!\n{e}");
            return ConfigFile::default();
        }
        match toml::from_str::<ConfigFile>(string.as_str()) {
            Ok(result) => result,
            Err(e) => {
                warn!("Failed to parse configuration file!\n{e}");
                ConfigFile::default()
            }
        }
    }

    pub fn load() -> Self {
        Self::load_config("config.toml")
    }

    fn load_config<P: AsRef<Path>>(path: P) -> Self {
        let configuration = Self::load_config_file(path);
        let domain_name = merge_config_sources(
            "LINUXWEB_DOMAIN_NAME",
            configuration.domain_name,
            None
        );
        Self {
            account_id: merge_config_sources(
                "LINUXWEB_ACCOUNT_ID",
                configuration.account_id,
                None
            ),
            password: merge_config_sources(
                "LINUXWEB_PASSWORD",
                configuration.password,
                None
            ),
            record_id: merge_config_sources(
                "LINUXWEB_RECORD_ID",
                configuration.record_id,
                None
            ),
            domain_id: merge_config_sources(
                "LINUXWEB_DOMAIN_ID",
                configuration.domain_id,
                None
            ),
            domain_name: domain_name.clone(),
            domain_ttl: merge_config_sources(
                "LINUXWEB_DOMAIN_TTL",
                configuration.domain_ttl,
                Some(600)
            ),
            ip_validate_path: merge_config_sources(
                "LINUXWEB_IP_VALIDATE_PATH",
                configuration.ip_validate_path,
                Some("https://".to_owned() + &domain_name + "/validate")
            ),
            ip_valid_response: merge_config_sources(
                "LINUXWEB_IP_VALID_RESPONSE",
                configuration.ip_valid_response,
                Some(domain_name)
            ),
            ip_fetch_path: merge_config_sources(
                "LINUXWEB_IP_FETCH_PATH",
                configuration.ip_fetch_path,
                Some("https://ipv4.icanhazip.com/".to_owned())
            ),
            secondary_ip_fetch_path: merge_config_sources(
                "LINUXWEB_SECONDARY_IP_FETCH_PATH",
                configuration.secondary_ip_fetch_path,
                Some("".to_owned())
            ),
            ip_check_interval_seconds: merge_config_sources(
                "LINUXWEB_IP_CHECK_INTERVAL_SECONDS",
                configuration.ip_check_interval_seconds,
                Some(60)
            ),
            ip_check_retry_interval_seconds: merge_config_sources(
                "LINUXWEB_IP_CHECK_RETRY_INTERVAL_SECONDS",
                configuration.ip_check_retry_interval_seconds,
                Some(5)
            ),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use serial_test::serial;

    fn wipe_environment() {
        env::remove_var("LINUXWEB_ACCOUNT_ID");
        env::remove_var("LINUXWEB_PASSWORD");
        env::remove_var("LINUXWEB_RECORD_ID");
        env::remove_var("LINUXWEB_DOMAIN_ID");
        env::remove_var("LINUXWEB_DOMAIN_NAME");
        env::remove_var("LINUXWEB_DOMAIN_TTL");
        env::remove_var("LINUXWEB_IP_VALID_RESPONSE");
        env::remove_var("LINUXWEB_IP_VALIDATE_PATH");
        env::remove_var("LINUXWEB_IP_FETCH_PATH");
        env::remove_var("LINUXWEB_SECONDARY_IP_FETCH_PATH");
        env::remove_var("LINUXWEB_IP_CHECK_INTERVAL_SECONDS");
        env::remove_var("LINUXWEB_IP_CHECK_RETRY_INTERVAL_SECONDS");
    }

    #[test]
    #[serial]
    #[should_panic]
    fn missing_required_values() {
        wipe_environment();
        Config::load_config("src/main.rs");
    }

    #[test]
    #[serial]
    fn everything_from_config() {
        wipe_environment();
        let config = Config::load_config("config.example.toml");
        assert_eq!(config, Config {
            account_id: "012345".to_string(),
            password: "xxx".to_string(),
            record_id: "12345".to_string(),
            domain_id: "1234".to_string(),
            domain_name: "example.com".to_string(),
            domain_ttl: 3600,
            ip_validate_path: "https://example.com/validate_here".to_string(),
            ip_valid_response: "validation response from server".to_string(),
            ip_fetch_path: "https://ipv6.icanhazip.com/".to_string(),
            secondary_ip_fetch_path: "https://icanhazip.com/".to_string(),
            ip_check_interval_seconds: 120,
            ip_check_retry_interval_seconds: 10,
        });
    }

    static REQUIRED_VARS: [(&str, &str); 5] = [
        ("LINUXWEB_ACCOUNT_ID", "123456"),
        ("LINUXWEB_PASSWORD", "password"),
        ("LINUXWEB_RECORD_ID", "11111"),
        ("LINUXWEB_DOMAIN_ID", "111111"),
        ("LINUXWEB_DOMAIN_NAME", "example.net")
    ];

    #[test]
    #[serial]
    #[should_panic]
    fn missing_required_value_account_id() {
        wipe_environment();
        set_optional_env_vars();
        for set_var in REQUIRED_VARS {
            if "LINUXWEB_ACCOUNT_ID" == set_var.0 {continue}
            env::set_var(set_var.0, set_var.1);
        }
        Config::load_config("src/main.rs");
    }

    #[test]
    #[serial]
    #[should_panic]
    fn missing_required_value_password() {
        wipe_environment();
        set_optional_env_vars();
        for set_var in REQUIRED_VARS {
            if "LINUXWEB_PASSWORD" == set_var.0 {continue}
            env::set_var(set_var.0, set_var.1);
        }
        Config::load_config("src/main.rs");
    }

    #[test]
    #[serial]
    #[should_panic]
    fn missing_required_value_record_id() {
        wipe_environment();
        set_optional_env_vars();
        for set_var in REQUIRED_VARS {
            if "LINUXWEB_RECORD_ID" == set_var.0 {continue}
            env::set_var(set_var.0, set_var.1);
        }
        Config::load_config("src/main.rs");
    }

    #[test]
    #[serial]
    #[should_panic]
    fn missing_required_value_domain_id() {
        wipe_environment();
        set_optional_env_vars();
        for set_var in REQUIRED_VARS {
            if "LINUXWEB_DOMAIN_ID" == set_var.0 {continue}
            env::set_var(set_var.0, set_var.1);
        }
        Config::load_config("src/main.rs");
    }

    #[test]
    #[serial]
    #[should_panic]
    fn missing_required_value_domain_name() {
        wipe_environment();
        set_optional_env_vars();
        for set_var in REQUIRED_VARS {
            if "LINUXWEB_DOMAIN_NAME" == set_var.0 {continue}
            env::set_var(set_var.0, set_var.1);
        }
        Config::load_config("src/main.rs");
    }

    fn set_required_env_vars() {
        for var in REQUIRED_VARS {
            env::set_var(var.0, var.1);
        }
    }

    fn set_optional_env_vars() {
        env::set_var("LINUXWEB_DOMAIN_TTL", "100");
        env::set_var("LINUXWEB_IP_VALID_RESPONSE", "valid response");
        env::set_var("LINUXWEB_IP_VALIDATE_PATH", "http://example.com/validate_here");
        env::set_var("LINUXWEB_IP_FETCH_PATH", "https://gimme.ip/v4");
        env::set_var("LINUXWEB_SECONDARY_IP_FETCH_PATH", "https://secondary.source/v4");
        env::set_var("LINUXWEB_IP_CHECK_INTERVAL_SECONDS", "10");
        env::set_var("LINUXWEB_IP_CHECK_RETRY_INTERVAL_SECONDS", "1");
    }

    #[test]
    #[serial]
    fn everything_from_environment() {
        set_required_env_vars();
        set_optional_env_vars();
        let config = Config::load_config("src/main.rs");
        assert_eq!(config, Config {
            account_id: "123456".to_string(),
            password: "password".to_string(),
            record_id: "11111".to_string(),
            domain_id: "111111".to_string(),
            domain_name: "example.net".to_string(),
            domain_ttl: 100,
            ip_validate_path: "http://example.com/validate_here".to_string(),
            ip_valid_response: "valid response".to_string(),
            ip_fetch_path: "https://gimme.ip/v4".to_string(),
            secondary_ip_fetch_path: "https://secondary.source/v4".to_string(),
            ip_check_interval_seconds: 10,
            ip_check_retry_interval_seconds: 1,
        })
    }

    #[test]
    #[serial]
    fn defaults() {
        wipe_environment();
        set_required_env_vars();
        let config = Config::load_config("src/main.rs");
        assert_eq!(config, Config {
            account_id: "123456".to_string(),
            password: "password".to_string(),
            record_id: "11111".to_string(),
            domain_id: "111111".to_string(),
            domain_name: "example.net".to_string(),
            domain_ttl: 600,
            ip_validate_path: "https://example.net/validate".to_string(),
            ip_valid_response: "example.net".to_string(),
            ip_fetch_path: "https://ipv4.icanhazip.com/".to_string(),
            secondary_ip_fetch_path: "".to_string(),
            ip_check_interval_seconds: 60,
            ip_check_retry_interval_seconds: 5,
        })
    }
}