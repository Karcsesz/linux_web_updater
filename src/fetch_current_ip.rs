use std::net::IpAddr;
use std::str::FromStr;
use log::{debug, error, info, warn};
use crate::config::Config;

/// Uses the server configured in `config.ip_fetch_path` to get the current public IP address of the machine
/// Returns `Some("IP")` on success, and `None` on failure
pub fn fetch_current_ip(config: &Config) -> Option<String> {
    match fetch_ip_from_path(&config.ip_fetch_path) {
        None => {
            warn!("Failed to fetch IP from primary source!");
            if !config.secondary_ip_fetch_path.is_empty() {
                match fetch_ip_from_path(&config.secondary_ip_fetch_path) {
                    None => {
                        error!("Failed to fetch IP from secondary source!");
                        None
                    },
                    Some(result) => Some(result)
                }
            } else {
                error!("No secondary source set, and primary source failed");
                None
            }
        },
        Some(result) => Some(result)
    }
}


fn fetch_ip_from_path(path: &str) -> Option<String> {
    debug!("Fetching current IP from {path}...");
    let response = match reqwest::blocking::get(path) {
        Ok(ok) => ok,
        Err(e) => {
            error!("Failed to fetch IP from external service!\n{e}");
            return None;
        }
    };
    debug!("Response code: {}", response.status());
    let response = response.text();
    match response {
        Ok(body) => {
            if IpAddr::from_str(body.trim()).is_err() {
                error!("Failed to parse {body} as a valid IP address");
                return None
            }
            info!("New IP: {body}");
            Some(body)
        },
        Err(e) => {
            error!("Failed to decode body of result:\n{e}");
            None
        }
    }
}
