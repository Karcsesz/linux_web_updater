mod update_record;
mod config;
mod fetch_current_ip;
mod validate_ip;

use std::thread::sleep;
use std::time::Duration;
use log::{error, info};
use crate::config::Config;
use crate::update_record::update_record;

fn main() {
    env_logger::init();
    let config = Config::load();
    if config.password == "xxx" {
        panic!("Please update config.toml to your actual domain information.");
    }
    loop {
        info!("Running check...");
        if !validate_ip::validate_ip(&config) {
            info!("Updating IP...");
            let new_ip = match fetch_current_ip::fetch_current_ip(&config) {
                None => {
                    error!("Failed to fetch current IP, retrying in {} seconds...", config.ip_check_retry_interval_seconds);
                    sleep(Duration::from_secs(config.ip_check_retry_interval_seconds));
                    continue;
                }
                Some(data) => data
            };
            update_record(&config, new_ip.as_str());
            info!("Updated IP!");
            info!("Waiting {} seconds to let DNS records expire", config.domain_ttl);
            sleep(Duration::from_secs(config.domain_ttl));
        }
        info!("Check complete!");
        sleep(Duration::from_secs(config.ip_check_interval_seconds));
    }
}
