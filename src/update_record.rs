use log::{debug, error, info, trace};
use crate::Config;

/// Logs in to the LinuxWeb servers and updates the record specified in `config` to the `new_ip` value
/// Returns `Some(())` on success and `None` on failure
pub fn update_record(config: &Config, new_ip: &str) -> Option<()> {
    debug!("Building client...");
    let client = match reqwest::blocking::Client::builder().cookie_store(true).build() {
        Ok(client) => client,
        Err(e) => {
            error!("Failed to create client for updating DNS record!\n{e}");
            return None
        }
    };
    info!("Logging in as {}", config.account_id);
    let login_form_params = [
        ("accountid", config.account_id.as_str()),
        ("password", config.password.as_str()),
        ("mode", "login"),
        ("submit_login", "belépés")
    ];
    let res = match client.post("https://online.linuxweb.hu/control.php").form(&login_form_params).send() {
        Ok(res) => res,
        Err(e) => {
            error!("Failed to log in to LinuxWeb control!\n{e}");
            return None
        }
    };
    info!("Response code: {}", res.status());
    trace!("Body: {:?}", res.text());
    info!("Updating domain {}", config.domain_name);
    let ttl = config.domain_ttl.to_string();
    let update_form_params = [
        ("valText", new_ip),
        ("ttl", ttl.as_str()),
        ("recordtype", "A"),
        ("record_id", config.record_id.as_str()),
        ("domain", config.domain_name.as_str()),
        ("domain_id", config.domain_id.as_str()),
        ("do_update", "Rekord+módosítása")
    ];
    let res = match client.post("https://online.linuxweb.hu/domain/list.php").form(&update_form_params).send() {
        Ok(res) => res,
        Err(e) => {
            error!("Failed to update domain information!\n{e}");
            return None
        }
    };
    info!("Response code: {}", res.status());
    trace!("Body: {:?}", res.text());
    info!("Generating new DNS records");
    let generate_form_params = [
        ("domain_id", config.domain_id.as_str()),
        ("do_dns_export", "DNS+generálás")
    ];
    let res = match client.post("https://online.linuxweb.hu/domain/list.php").form(&generate_form_params).send() {
        Ok(res) => res,
        Err(e) => {
            error!("Failed to generate domain information!\n{e}");
            return None
        }
    };
    info!("Response code: {}", res.status());
    trace!("Body: {:?}", res.text());
    Some(())
}