use log::{debug, error};
use crate::config::Config;

/// Checks if the domain in `config.ip_validate_path` returns a valid response
/// Returns the value of `response.text().trim() == config.ip_valid_response.trim()`
pub fn validate_ip(config: &Config) -> bool {
    debug!("Validating current IP...");
    let response = match reqwest::blocking::get(&config.ip_validate_path) {
        Ok(resp) => resp,
        Err(e) => {
            error!("Failed to connect to server for IP validation!\n{e}");
            return false;
        }
    };
    debug!("Response code is {}", response.status());
    if let Ok(response) = response.text() {
        debug!("Response body is {response}");
        debug!("Comparing with {}", config.ip_valid_response);
        response.trim() == config.ip_valid_response.trim()
    } else {
        false
    }
}
