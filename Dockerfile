FROM rust as builder

RUN rustup target add x86_64-unknown-linux-musl
RUN apt update && apt install -y musl-tools musl-dev
RUN update-ca-certificates

ENV USER=linuxweb
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nohome" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid ${UID} \
    ${USER}

WORKDIR /usr/src/myapp
COPY . .

RUN cargo build --target x86_64-unknown-linux-musl --release --features=rustls --no-default-features

FROM scratch

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

WORKDIR /usr/src/myapp

COPY --from=builder /usr/src/myapp/target/x86_64-unknown-linux-musl/release/linux_web_updater .

USER linuxweb:linuxweb

ENV RUST_LOG="info"

CMD ["/usr/src/myapp/linux_web_updater"]