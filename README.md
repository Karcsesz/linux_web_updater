# LinuxWeb DynDNS updater

A utility for automatically updating [LinuxWeb](https://linuxweb.hu/) DNS records through their web interface.

Currently IPv4 only!

## Configuration
The application requires the following data:
 - User ID and password used to log in
 - [Domain and record ID](#getting-the-domain-and-record-ids)
 - Domain name

It also needs an endpoint hosted through a webserver on the same domain which returns a known static value

To start configuration, copy `config.example.toml` to `config.toml` and update the values marked with `#CHANGE`. You can also use environment variables with the form `LINUXWEB_NAME_IN_CONFIGURATION_FILE`.

### Configuration values
| Name in configuration             | Environment variable name                  | Default value                  |
|-----------------------------------|--------------------------------------------|--------------------------------|
| `account_id`                      | `LINUXWEB_ACCOUNT_ID`                      | Required                       |
| `password`                        | `LINUXWEB_PASSWORD`                        | Required                       |
| `record_id`                       | `LINUXWEB_RECORD_ID`                       | Required                       |
| `domain_id`                       | `LINUXWEB_DOMAIN_ID`                       | Required                       |
| `domain_name`                     | `LINUXWEB_DOMAIN_NAME`                     | Required                       |
| `domain_ttl`                      | `LINUXWEB_DOMAIN_TTL`                      | 600                            |
| `ip_valid_response`               | `LINUXWEB_IP_VALID_RESPONSE`               | `domain_name`                  |
| `ip_validate_path`                | `LINUXWEB_IP_VALIDATE_PATH`                | `https://domain_name/validate` |
| `ip_fetch_path`                   | `LINUXWEB_IP_FETCH_PATH`                   | "https://ipv4.icanhazip.com/"  |
| `secondary_ip_fetch_path`         | `LINUXWEB_SECONDARY_IP_FETCH_PATH`         | "" (Disabled)                  |
| `ip_check_interval_seconds`       | `LINUXWEB_IP_CHECK_INTERVAL_SECONDS`       | 60                             |
| `ip_check_retry_interval_seconds` | `LINUXWEB_IP_CHECK_RETRY_INTERVAL_SECONDS` | 5                              |

## Getting the domain and record IDs
1. Log in to https://online.linuxweb.hu
2. Navigate to the domain records
3. Start editing the `A` record to be updated (click the :pencil2: icon)
4. The website's URL will be updated to show both IDs:
`https://online.linuxweb.hu/domain/list.php?record_id={record id}&mod=1&domain_id={domain id}`

## Working principle
1. Every `ip_check_interval_seconds`, send an HTTP GET request to `ip_validate_path`.
2. Compare the response body with `ip_valid_response`, stripping both of starting and ending whitespace beforehand.
3. If the values don't match:
   1. Send a request to `ip_fetch_path` to get the current public IP.
   2. Log in to LinuxWeb with credentials.
   3. Use credentials to update the `record_id` record of `domain_id` domain with the new IP address.
      - Doesn't fetch current TTL value and domain name, just overwrites it with values in `config.toml`
   4. Pause loop for `domain_ttl` seconds to let records expire

## Features
 - `default-tls`: Use the default TLS implementation for `reqwest` (enabled by default)
 - `rustls`: Use `rustls` for TLS in `reqwest`